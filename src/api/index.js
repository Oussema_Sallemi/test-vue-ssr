import axios from "axios";

const instance = axios.create({
  baseURL: "http://172.17.0.5:3500/",
});

const getProducts = () => instance.get("/api/products");

export default {
  getProducts,
};
