import Vue from "vue";
import Router from "vue-router";
import Entreprise from "../components/entreprise/index.vue";
import Home from "../pages/index.vue";
import Product from "../components/product/index.vue";
import Category from "../components/category/index.vue";
import Not_Found from "../pages/error.vue";

Vue.use(Router);

export function createRouter() {
  const routes = [
    {
      path: "/",
      component: Home,
      name: "home",
    },
    {
      path: "/entreprise",
      component: Entreprise,
      name: "entreprise",
    },
    {
      path: "/product",
      component: Product,
      name: "product",
    },
    {
      path: "/category",
      component: Category,
      name: "category",
    },
    {
      path: "/404",
      component: Not_Found,
      name: "not_found",
    },
  ];
  const router = new Router({
    mode: "history",
    routes: routes,
  });
  return router;
}
