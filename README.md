## PS : the state is not perisiatnt in this project , so we will face some problems and bugs untill we can implement state persistancy

## to run the project you need to create an admin token and pass it inside .env file like so : VUE_APP_TOKEN=26jhwy850vg5b6b56km9208dyd7zofxx

## it needs to have the format of VUE*APP*\* to be used inside a vue js project

## use this command inside postamn (if available) to create an admin token : https://migration.lepape.com/rest/default/V1/integration/admin/token [with a POST reques]

## how to create a local mongodb database (I used a local mongodb database because the file I am going to upload to the db is very large for a weak internet connection to handle):

## to install mongodb on ubuntu , open terminal and follow installation process in this link : https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

## satrt mongo db server on ubuntu : sudo service mongod start

## use this command to connect to mongo shell : mongo

## to show available databases : show dbs

## to switch to a database : use database_name

## see database you are using currently : db;

## import url_keys.json file inside a database : mongoimport --host 127.0.0.1 --db "url_keys" --collection "keys" --jsonArray --file '/full path to folder in which file exists/lepape_preprod_url_rewrite.json' --mode merge

## add the database url to your.env file : DATABASE_URL=mongodb://localhost:27017/node-express-mongodb-server

## to worki inside the container : docker exec -it pwa sh
